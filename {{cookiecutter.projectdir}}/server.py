import os
from django.core.wsgi import get_wsgi_application
from waitress import serve
from whitenoise import WhiteNoise

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "{{cookiecutter.appdir}}.settings")

application = WhiteNoise(get_wsgi_application(),
                         root='/public/media', prefix='media/')

if __name__ == '__main__':
    serve(application,
          host=os.environ.get('HOST', '127.0.0.1'),
          port=os.environ.get('PORT', '5000'))
