#!/bin/bash

docker run -d \
       --restart unless-stopped \
       --name {{cookiecutter.imagename}} \
       --env-file ./production.env \
       registry.tompaton.com/{{cookiecutter.username}}/{{cookiecutter.imagename}}
